class RootController < ApplicationController

  def index
    value = CGI.escape("Cookies are working " + Time.now.utc.to_s)
    response.headers["Set-Cookie"] = "cookie-test=%s; Path=/; Secure; HttpOnly" % value
  end

  def reset
    cookies.clear
    redirect_to root_path
  end

end
